package com.brom.epam.task11;

import com.brom.epam.task11.view.View;

public class Application {
  public static void main(String[] args) {
    new View().show();
  }
}
