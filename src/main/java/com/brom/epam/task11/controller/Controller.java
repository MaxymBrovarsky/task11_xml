package com.brom.epam.task11.controller;

import com.brom.epam.task11.model.Medicine;
//import com.brom.epam.task11.service.DOMParser;
import com.brom.epam.task11.service.DOMParser;
import com.brom.epam.task11.service.MedicinesHolder;
import com.brom.epam.task11.service.SAXHandler;
import com.brom.epam.task11.util.XMLToHTMLConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Controller {
  public List<Medicine> showDOMParserExample(String filePath)
      throws IOException, SAXException, ParserConfigurationException {
    DOMParser domParser = new DOMParser();
    List<Medicine> medicines = domParser.parseDocument(filePath);
    return medicines;
  }
  public List<Medicine> showSAXParserExample(String filePath)
      throws IOException, SAXException, ParserConfigurationException {
    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
    SchemaFactory factory = SchemaFactory.newInstance(language);
    Schema schema = factory.newSchema(new File("F:\\EPAM\\task11\\src\\main\\resources\\MedicinesSchema.xsd"));
    saxParserFactory.setSchema(schema);

    SAXParser saxParser = saxParserFactory.newSAXParser();
    SAXHandler saxHandler = new SAXHandler();
    saxParser.parse(new File(filePath), saxHandler);
    return saxHandler.getMedicines();
  }

  public List<Medicine> showStAXParserExample(String filePath)
      throws FileNotFoundException, XMLStreamException {
    MedicinesHolder medicinesHolder = new MedicinesHolder();
    XMLInputFactory factory = XMLInputFactory.newInstance();
    XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(filePath));
    String currentTagName = "";
    while (eventReader.hasNext()) {
      XMLEvent event = eventReader.nextEvent();
      switch (event.getEventType()) {
        case XMLStreamConstants.START_ELEMENT:
          StartElement startElement = event.asStartElement();
          currentTagName = startElement.getName().getLocalPart();
          break;
        case XMLStreamConstants.CHARACTERS:
          medicinesHolder.processElement(currentTagName.toLowerCase(), event.asCharacters().getData());
          break;
        case XMLStreamConstants.END_ELEMENT:
          currentTagName = "";
      }
    }
    return medicinesHolder.getMedicines();
  }

  public void convertXMLToHTML(String filePath, String schemaPath) {
    Source xml = new StreamSource(new File(filePath));
    Source xslt = new StreamSource(new File(schemaPath));
    XMLToHTMLConverter.convertXMLToHTML(xml, xslt);
  }

  public void changeRootElement(String filePath)
      throws ParserConfigurationException, IOException, SAXException, TransformerException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(new File(filePath));
    Element element = document.getDocumentElement();
    Element element1 = document.createElement("renamedRoot");
    while (element.hasChildNodes()) {
      element1.appendChild(element.getFirstChild());
    }
    element.getParentNode().replaceChild(element1, element);
    TransformerFactory tranFactory = TransformerFactory.newInstance();
    Transformer aTransformer = tranFactory.newTransformer();
    Source src = new DOMSource(document);
    Result dest = new StreamResult(new File("xmlFileName.xml"));
    aTransformer.transform(src, dest);
  }
}
