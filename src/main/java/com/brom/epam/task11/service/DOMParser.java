package com.brom.epam.task11.service;

import com.brom.epam.task11.model.Certificate;
import com.brom.epam.task11.model.Dosage;
import com.brom.epam.task11.model.Medicine;
import com.brom.epam.task11.model.Package;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DOMParser {
  private MedicinesHolder medicinesHolder;

  public DOMParser() {
    this.medicinesHolder = new MedicinesHolder();
  }
  public List<Medicine> parseDocument(String path)
      throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(new File(path));
    NodeList medicineNodes = document.getDocumentElement().getElementsByTagName("Medicine");
    for (int i = 0; i < medicineNodes.getLength(); i++) {
      Node medicine = medicineNodes.item(i);
      NodeList childNodes = medicine.getChildNodes();
      for (int j = 0; j < childNodes.getLength(); j++) {
        Node element = childNodes.item(j);
        if (element.getNodeType() == Node.ELEMENT_NODE) {
          String elementName = ((Element) element).getTagName().toLowerCase();
          medicinesHolder.processElement(elementName, element.getTextContent());
        }
        if (element.hasChildNodes()) {
          NodeList child = element.getChildNodes();
          for (int k = 0; k < child.getLength(); k++) {
            Node childElement = child.item(k);
            if (childElement.getNodeType() == Node.ELEMENT_NODE) {
              String elementName = ((Element) childElement).getTagName().toLowerCase();
              medicinesHolder.processElement(elementName, childElement.getTextContent());
            }
          }
        }
      }
    }
    return medicinesHolder.getMedicines();
  }
}
