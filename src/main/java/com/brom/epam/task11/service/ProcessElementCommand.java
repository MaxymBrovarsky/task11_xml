package com.brom.epam.task11.service;

import com.brom.epam.task11.model.Medicine;
import org.w3c.dom.Node;

public interface ProcessElementCommand {
  void execute(String node, Medicine medicine);
}
