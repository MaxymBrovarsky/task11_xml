package com.brom.epam.task11.service;

import com.brom.epam.task11.model.Medicine;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {
  private MedicinesHolder medicinesHolder = new MedicinesHolder();
  private Map<String, Boolean> currentElement;

  public SAXHandler() {
    currentElement = new HashMap<>();
    currentElement.put("name", false);
    currentElement.put("analog", false);
    currentElement.put("dosage", false);
    currentElement.put("group", false);
    currentElement.put("package", false);
    currentElement.put("pharm", false);
    currentElement.put("version", false);
    currentElement.put("certificate", false);
    currentElement.put("id", false);
    currentElement.put("dateofissue", false);
    currentElement.put("enddate", false);
    currentElement.put("originorganization", false);
    currentElement.put("amountforoneuse", false);
    currentElement.put("frequency", false);
    currentElement.put("type", false);
    currentElement.put("amountin", false);
    currentElement.put("price", false);
  }
  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    System.out.println(qName);
    Optional<Entry<String, Boolean>> optional = currentElement.entrySet().stream()
            .filter(e -> e.getKey().equalsIgnoreCase(qName))
            .findFirst();
    if (optional.isPresent()) {
      optional.get().setValue(true);
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    currentElement.entrySet().forEach(e -> e.setValue(false));
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    Optional<Entry<String, Boolean>> activeElement =  currentElement.entrySet().stream()
        .filter(e -> e.getValue().booleanValue())
        .findFirst();
    if (activeElement.isPresent()) {
      medicinesHolder.processElement(activeElement.get().getKey(), new String(ch, start, length).toLowerCase());
    }
  }

  public List<Medicine> getMedicines() {
    return this.medicinesHolder.getMedicines();
  }
}
