package com.brom.epam.task11.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;

public class XMLToHTMLConverter {
  public static void convertXMLToHTML(Source xml, Source xslt) {
    StringWriter sw = new StringWriter();
    try {
      FileWriter fw = new FileWriter("src\\main\\resources\\medicines.html");
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer trasform = tFactory.newTransformer(xslt);
      trasform.transform(xml, new StreamResult(sw));
      fw.write(sw.toString());
      fw.close();
    } catch (IOException | TransformerConfigurationException e) {
      e.printStackTrace();
    } catch (TransformerFactoryConfigurationError e) {
      e.printStackTrace();
    } catch (TransformerException e) {
      e.printStackTrace();
    }
  }
}
