package com.brom.epam.task11.view;

public interface Command {
  void execute();
}
