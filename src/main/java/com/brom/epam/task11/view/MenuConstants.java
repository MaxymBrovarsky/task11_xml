package com.brom.epam.task11.view;

public class MenuConstants {
  public static final String DOM_PARSER_KEY = "1";
  public static final String DOM_PARSER_TEXT = "Show dom parser example";
  public static final String SAX_PARSER_KEY = "2";
  public static final String SAX_PARSER_TEXT = "Show sax parser example";
  public static final String STAX_PARSER_KEY = "3";
  public static final String STAX_PARSER_TEXT = "Show StAX parser example";
  public static final String XML_TO_HTML_KEY = "4";
  public static final String XML_TO_HTML_TEXT = "Transform XML to HTML";
  public static final String CHANGE_ROOT_KEY = "5";
  public static final String CHANGE_ROOT_TEXT = "Change root element";
}
