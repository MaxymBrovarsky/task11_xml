package com.brom.epam.task11.view;

import com.brom.epam.task11.controller.Controller;
import com.brom.epam.task11.model.Medicine;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

public class View {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private Controller controller;

  public View() {
    this.initMenu();
    this.initMenuCommands();
    this.controller = new Controller();
  }

  private void initMenu() {
    this.menu = new HashMap<>();
    this.menu.put(MenuConstants.DOM_PARSER_KEY,
        getMenuOptionText(MenuConstants.DOM_PARSER_KEY, MenuConstants.DOM_PARSER_TEXT));
    this.menu.put(MenuConstants.SAX_PARSER_KEY,
        getMenuOptionText(MenuConstants.SAX_PARSER_KEY, MenuConstants.SAX_PARSER_TEXT));
    this.menu.put(MenuConstants.STAX_PARSER_KEY,
        getMenuOptionText(MenuConstants.STAX_PARSER_KEY, MenuConstants.STAX_PARSER_TEXT));
    this.menu.put(MenuConstants.XML_TO_HTML_KEY,
        getMenuOptionText(MenuConstants.XML_TO_HTML_KEY, MenuConstants.XML_TO_HTML_TEXT));
    this.menu.put(MenuConstants.CHANGE_ROOT_KEY,
        getMenuOptionText(MenuConstants.CHANGE_ROOT_KEY, MenuConstants.CHANGE_ROOT_TEXT));
  }

  private String getMenuOptionText(String key, String text) {
    return key + ". " + text;
  }

  private void initMenuCommands() {
    this.menuCommands = new HashMap<>();
    this.menuCommands.put(MenuConstants.DOM_PARSER_KEY, this::showDOMParserExample);
    this.menuCommands.put(MenuConstants.SAX_PARSER_KEY, this::showSAXParserExample);
    this.menuCommands.put(MenuConstants.STAX_PARSER_KEY, this::showSTAXParserExample);
    this.menuCommands.put(MenuConstants.XML_TO_HTML_KEY, this::showXMLToHTMLExample);
    this.menuCommands.put(MenuConstants.CHANGE_ROOT_KEY, this::showChangeRootExample);
  }

  private void showDOMParserExample() {
    String filePath = input.nextLine();
    try {
      List<Medicine> medicines = this.controller.showDOMParserExample(filePath);
      medicines.forEach(m -> logger.info(m));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }

  private void showSAXParserExample() {
    String filePath = input.nextLine();
    try {
      List<Medicine> medicines = this.controller.showSAXParserExample(filePath);
      medicines.forEach(m -> logger.info(m));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }

  private void showSTAXParserExample() {
    String filePath = input.nextLine();
    List<Medicine> medicines = null;
    try {
      medicines = this.controller.showStAXParserExample(filePath);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (XMLStreamException e) {
      e.printStackTrace();
    }
    medicines.forEach(m -> logger.info(m));
  }

  private void showXMLToHTMLExample() {
    String filePath = input.nextLine();
    String schemaPath = input.nextLine();
    controller.convertXMLToHTML(filePath, schemaPath);
  }

  private void showChangeRootExample() {
    String filePath = input.nextLine();
    try {
      controller.changeRootElement(filePath);
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (TransformerException e) {
      e.printStackTrace();
    }
  }

  public void show() {
    while(true) {
      this.menu.values().forEach(v -> logger.info(v));
      String commandKey = input.nextLine();
      this.menuCommands.get(commandKey).execute();
    }
  }
}
