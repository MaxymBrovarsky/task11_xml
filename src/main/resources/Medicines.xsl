<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          table.tfmt {
          border: 1px ;
          }

          td.colfmt {
          border: 1px ;
          background-color: yellow;
          color: red;
          text-align:right;
          }

          th {
          background-color: #2E9AFE;
          color: white;
          }

        </style>
      </head>

      <body>
        <table class="tfmt">
          <tr>
            <th style="width:250px">Name</th>
            <th style="width:350px">Pharm</th>
            <th style="width:250px">Group</th>
            <th style="width:250px">Analog</th>
            <th style="width:250px">Version</th>
            <th style="width:250px">CertificateId</th>
            <th style="width:250px">CertificateDateOfIssue</th>
            <th style="width:250px">CertificateEndDate</th>
            <th style="width:250px">CertificateOriginOrganization</th>
            <th style="width:250px">PackageType</th>
            <th style="width:250px">PackageAmountIn</th>
            <th style="width:250px">PackagePrice</th>
            <th style="width:250px">DosageForOneUse</th>
            <th style="width:250px">DosageDescription</th>
          </tr>
          <xsl:for-each select="Medicines/Medicine">
            <xsl:sort select="Name"/>
            <tr>
              <td class="colfmt">
                <xsl:value-of select="Name" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Pharm" />
              </td>

              <td class="colfmt">
                <xsl:value-of select="Group" />
              </td>
              <td class="colfmt">
                <xsl:for-each select="Analog">
                  <xsl:value-of select="current()" />
                </xsl:for-each>
              </td>
              <td class="colfmt">
                <xsl:value-of select="Version" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Certificate/Id" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Certificate/DateOFIssue" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Certificate/EndDate" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Certificate/OriginOrganization" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Package/Type" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Package/AmountIn" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Package/Price" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Dosage/AmountForOneUse" />
              </td>
              <td class="colfmt">
                <xsl:value-of select="Dosage/Frequency" />
              </td>

            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>